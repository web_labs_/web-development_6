import React from 'react';
import { render } from '@testing-library/react';
import { it, expect } from '@jest/globals';
import { Card } from '../components';

it('Компонент Card должен принимать пропс name и отображать его', () => {
  const cardName = 'Покормить кота';

  const { container } = render(<Card name={cardName} />);

  expect(container).toHaveTextContent(cardName);
});
