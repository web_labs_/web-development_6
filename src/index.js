import reportWebVitals from './reportWebVitals';

/**
 * Нужно сделать так, чтобы React отрендерил свое дерево.
 * См. инициализацию React дерева.
 * https://ru.reactjs.org/docs/hello-world.html
 */

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
